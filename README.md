# Android RxJava Retrofit2 Dagger2 Sample Project

Sample Project to show you, how you could use RxJava, Retrofit2, Dagger2, SQLBrite, GSON, AutoValue, Timber and other helpful libraries to handle network requests. It also demonstrates the MVP architecture and tools that I use when developing for the Android platform (http://fakegeek.ru).
To build this project, you will need a New Your Times API Key. You can get it [here](http://developer.nytimes.com/) and put it in your `gradle.properties` file.

![1.jpg](https://bitbucket.org/repo/6Lq6Ep/images/4171971150-1.jpg) ![2.jpg](https://bitbucket.org/repo/6Lq6Ep/images/3719601377-2.jpg) ![3.jpg](https://bitbucket.org/repo/6Lq6Ep/images/2239457649-3.jpg)

Libraries and tools included:

- Support libraries
- RecyclerViews, CardViews and CustomTabs
- [RxJava](https://github.com/ReactiveX/RxJava) and [RxAndroid](https://github.com/ReactiveX/RxAndroid) 
- [Retrolambda](https://github.com/evant/gradle-retrolambda) 
- [Retrofit 2](http://square.github.io/retrofit/)
- [Dagger 2](http://google.github.io/dagger/)
- [SqlBrite](https://github.com/square/sqlbrite)
- [Stetho](http://facebook.github.io/stetho/)
- [Butterknife](https://github.com/JakeWharton/butterknife)
- [Jake Wharton's Hugo](https://github.com/JakeWharton/hugo)
- [Dexter](https://github.com/Karumi/Dexter)
- [OkHttp3](https://github.com/square/okhttp)
- [Timber](https://github.com/JakeWharton/timber)
- [Glide](https://github.com/bumptech/glide)
- [AutoValue](https://github.com/google/auto/tree/master/value) with extensions [AutoValueParcel](https://github.com/rharter/auto-value-parcel) and [AutoValueGson](https://github.com/rharter/auto-value-gson)
- Functional tests with [Espresso](https://google.github.io/android-testing-support-library/docs/espresso/index.html)
- [Robolectric](http://robolectric.org/)
- [Mockito](http://mockito.org/)
- [Checkstyle](http://checkstyle.sourceforge.net/), [PMD](https://pmd.github.io/) and [Findbugs](http://findbugs.sourceforge.net/) for code analysis

## Requirements

- JDK 1.8
- [Android SDK](http://developer.android.com/sdk/index.html).
- Android N [(API 24) ](http://developer.android.com/tools/revisions/platforms.html).
- Latest Android SDK Tools and build tools.


## Architecture

This project follows MVP's Android architecture guidelines (https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93presenter). Read more about them [here](http://antonioleiva.com/mvp-android/). 

**NOTE:** Some say, that you just can't use boilerplate code to start new project.. Well, it's not true. I like programming and I don't like to write a lot of boilerplate code (same as you, I suppose). In the web, for instance, they have Twitter Bootstrap for front-end, and all the people around use it just because it's useful and easy to use, so you can concentrate on some other goals. 

This project uses [ribot's boilerplate code](https://github.com/ribot/android-boilerplate) as a base. Because this is the best base you could have for your project (IMO). So if you still didn't see it, go for it!

**What changes does this project has against the boilerplate code:**

- I don't think this is a good idea to use only one data manager for the entire app. So I decided to split that logic according to every logical-module.
- Every logical module has its own package. Which means, if you need something, you always know where to find it. For instance, if you want to change something on a login screen, you go to a login package. And even more, if you'd like delete some particular module (for some reason), you just need to delete the package of the module. (There is [a good article](http://programmingisterrible.com/post/139222674273/write-code-that-is-easy-to-delete-not-easy-to) about that).
- ENTIRE PACKAGE WITH ITS OWN LOGIC: nytreviews
- DEBUG and RELEASE versions of ApiCreatorHelper and StethoHelper
- added several cool libraries, like stetho, retrolambda, hugo, etc.


## License

```
    Copyright 2016 fakeGEEK (Sergey Antonov) http://fakegeek.ru.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
```