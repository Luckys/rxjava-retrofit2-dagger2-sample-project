package ru.fakegeek.rxretrodagsample;

import org.junit.Rule;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import ru.fakegeek.rxretrodagsample.core.data.local.DbOpenHelper;
import ru.fakegeek.rxretrodagsample.nytreviews.data.local.NewYorkTimesDbHelper;
import ru.fakegeek.rxretrodagsample.util.DefaultConfig;
import ru.fakegeek.rxretrodagsample.util.RxSchedulersOverrideRule;

/**
 * Unit tests integration with a SQLite Database using Robolectric
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = DefaultConfig.EMULATE_SDK)
public class ReviewsDbHelperTest {

    private final NewYorkTimesDbHelper mDatabaseHelper =
            new NewYorkTimesDbHelper(new DbOpenHelper(RuntimeEnvironment.application));

    @Rule
    public final RxSchedulersOverrideRule mOverrideSchedulersRule = new RxSchedulersOverrideRule();



}