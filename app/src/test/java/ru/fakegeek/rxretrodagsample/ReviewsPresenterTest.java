package ru.fakegeek.rxretrodagsample;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import ru.fakegeek.rxretrodagsample.nytreviews.data.NewYorkTimesDataManager;
import ru.fakegeek.rxretrodagsample.nytreviews.ui.ReviewsPresenter;
import ru.fakegeek.rxretrodagsample.nytreviews.ui.ReviewsView;
import ru.fakegeek.rxretrodagsample.util.RxSchedulersOverrideRule;

@RunWith(MockitoJUnitRunner.class)
public class ReviewsPresenterTest {

    @Mock
    ReviewsView mMockReviewsView;
    @Mock
    NewYorkTimesDataManager mNytDataManager;
    private ReviewsPresenter mReviewsPresenter;

    @Rule
    public final RxSchedulersOverrideRule mOverrideSchedulersRule = new RxSchedulersOverrideRule();

    @Before
    public void setUp() {
        mReviewsPresenter = new ReviewsPresenter(mNytDataManager);
        mReviewsPresenter.attachView(mMockReviewsView);
    }

    @After
    public void tearDown() {
        mReviewsPresenter.detachView();
    }



}