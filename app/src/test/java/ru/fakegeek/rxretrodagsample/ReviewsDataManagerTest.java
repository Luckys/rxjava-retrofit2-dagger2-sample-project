package ru.fakegeek.rxretrodagsample;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import ru.fakegeek.rxretrodagsample.core.data.local.PreferencesHelper;
import ru.fakegeek.rxretrodagsample.nytreviews.data.NewYorkTimesDataManager;
import ru.fakegeek.rxretrodagsample.nytreviews.data.local.NewYorkTimesDbHelper;
import ru.fakegeek.rxretrodagsample.nytreviews.data.remote.ApiNewYorkTimes;


@RunWith(MockitoJUnitRunner.class)
public class ReviewsDataManagerTest {

    @Mock
    NewYorkTimesDbHelper mMockDatabaseHelper;
    @Mock PreferencesHelper mMockPreferencesHelper;
    @Mock
    ApiNewYorkTimes mMockApiNyt;
    private NewYorkTimesDataManager mNytDataManager;
}
