package ru.fakegeek.rxretrodagsample.util;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 *
 * Created by serg on 10.09.16.
 */
public class StethoHelper {

    public static void install(Application application) {
        Stetho.initialize(
                Stetho.newInitializerBuilder(application)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(application))
                        .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(application))
                        .build());

    }
}
