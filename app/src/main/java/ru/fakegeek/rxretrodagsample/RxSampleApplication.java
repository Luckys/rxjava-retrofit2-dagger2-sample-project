package ru.fakegeek.rxretrodagsample;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import ru.fakegeek.rxretrodagsample.injection.component.ApplicationComponent;
import ru.fakegeek.rxretrodagsample.injection.component.DaggerApplicationComponent;
import ru.fakegeek.rxretrodagsample.injection.module.ApplicationModule;
import ru.fakegeek.rxretrodagsample.util.StethoHelper;
import timber.log.Timber;

public class RxSampleApplication extends Application  {

    ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
            Fabric.with(this, new Crashlytics());
        }
        StethoHelper.install(this);
    }

    public static RxSampleApplication get(Context context) {
        return (RxSampleApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return mApplicationComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }
}
