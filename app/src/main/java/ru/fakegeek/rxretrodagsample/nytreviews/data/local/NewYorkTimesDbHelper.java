package ru.fakegeek.rxretrodagsample.nytreviews.data.local;

import android.database.sqlite.SQLiteDatabase;

import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import ru.fakegeek.rxretrodagsample.core.data.local.DbOpenHelper;
import ru.fakegeek.rxretrodagsample.nytreviews.data.model.NewYorkTimesReview;
import rx.Observable;
import rx.Subscriber;

/**
 *
 * Created by Antonov.Sergey6 on 16.08.2016.
 */
public class NewYorkTimesDbHelper {

    private final BriteDatabase mDb;

    @Inject
    public NewYorkTimesDbHelper(DbOpenHelper dbOpenHelper) {
        mDb = SqlBrite.create().wrapDatabaseHelper(dbOpenHelper);
    }

    public BriteDatabase getBriteDb() {
        return mDb;
    }

    public Observable<NewYorkTimesReview> setNewYorkTimesReviews(
            final Collection<NewYorkTimesReview> newYorkTimesReviews) {
        return Observable.create(new Observable.OnSubscribe<NewYorkTimesReview>() {
            @Override
            public void call(Subscriber<? super NewYorkTimesReview> subscriber) {
                if (subscriber.isUnsubscribed()) return;
                    BriteDatabase.Transaction transaction = mDb.newTransaction();
                    try {
                        mDb.delete(NewYorkTimesTable.TABLE_NAME, null);
                        for (NewYorkTimesReview newYorkTimesReview : newYorkTimesReviews) {
                            long result = mDb.insert(NewYorkTimesTable.TABLE_NAME,
                                    NewYorkTimesTable.toContentValues(newYorkTimesReview),
                                    SQLiteDatabase.CONFLICT_REPLACE);
                            if (result >= 0) subscriber.onNext(newYorkTimesReview);
                        }
                        transaction.markSuccessful();
                        subscriber.onCompleted();
                    } finally {
                        transaction.end();
                    }
            }
        });
    }

    public Observable<List<NewYorkTimesReview>> getNewYorkTimesReviews() {
        return mDb.createQuery(NewYorkTimesTable.TABLE_NAME,
                "SELECT * FROM " + NewYorkTimesTable.TABLE_NAME)
                .mapToList(NewYorkTimesTable::parseCursor);
    }

    public void deleteNewYorkTimesReviews() {
        BriteDatabase.Transaction transaction = mDb.newTransaction();
        try {
            mDb.delete(NewYorkTimesTable.TABLE_NAME, null);
            transaction.markSuccessful();
        } finally {
            transaction.end();
        }
    }
}
