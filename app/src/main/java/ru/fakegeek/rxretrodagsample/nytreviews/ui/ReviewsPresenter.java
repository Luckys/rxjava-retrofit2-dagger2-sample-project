package ru.fakegeek.rxretrodagsample.nytreviews.ui;

import java.util.List;

import javax.inject.Inject;

import retrofit2.adapter.rxjava.HttpException;
import ru.fakegeek.rxretrodagsample.core.ui.base.BasePresenter;
import ru.fakegeek.rxretrodagsample.injection.ConfigPersistent;
import ru.fakegeek.rxretrodagsample.nytreviews.data.NewYorkTimesDataManager;
import ru.fakegeek.rxretrodagsample.nytreviews.data.model.NewYorkTimesReview;
import ru.fakegeek.rxretrodagsample.nytreviews.data.model.Results;
import ru.fakegeek.rxretrodagsample.util.RxUtil;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

@ConfigPersistent
public class ReviewsPresenter extends BasePresenter<ReviewsView> {

    // --------------------------------------------------------------------------------------------
    //      FIELDS
    // --------------------------------------------------------------------------------------------

    private final NewYorkTimesDataManager mNewYorkTimesDataManager;
    private Subscription mSubscription;

    // --------------------------------------------------------------------------------------------
    //      CONSTRUCTOR
    // --------------------------------------------------------------------------------------------

    @Inject
    public ReviewsPresenter(NewYorkTimesDataManager newYorkTimesDataManager) {
        mNewYorkTimesDataManager = newYorkTimesDataManager;
    }

    // --------------------------------------------------------------------------------------------
    //      LIFECYCLE
    // --------------------------------------------------------------------------------------------

    @Override
    public void attachView(ReviewsView reviewsView) {
        super.attachView(reviewsView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) mSubscription.unsubscribe();
    }

    // --------------------------------------------------------------------------------------------
    //      PUBLIC METHODS
    // --------------------------------------------------------------------------------------------

    public void loadReviews(String searchQuery) {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscription);
        mSubscription = mNewYorkTimesDataManager.syncNewYorkTimesReviews(searchQuery)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Results>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "There was an error loading the reviews.");
                        getMvpView().stopRefreshing();
                        if (e instanceof HttpException) {
                            getMvpView().showError(((HttpException) e).message());
                        } else {
                            getMvpView().showError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(Results results) {
                        getMvpView().stopRefreshing();
                        List<NewYorkTimesReview> reviews = results.results();
                        if (reviews.isEmpty()) {
                            getMvpView().showReviewsEmpty();
                        } else {
                            getMvpView().showReviews(reviews);
                        }
                    }
                });
    }

    public void openLink(NewYorkTimesReview review) {
        getMvpView().openLink(review);
    }
}
