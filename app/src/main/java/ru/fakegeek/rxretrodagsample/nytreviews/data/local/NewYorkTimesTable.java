package ru.fakegeek.rxretrodagsample.nytreviews.data.local;

import android.content.ContentValues;
import android.database.Cursor;

import ru.fakegeek.rxretrodagsample.nytreviews.data.model.MultimediaPhoto;
import ru.fakegeek.rxretrodagsample.nytreviews.data.model.NewYorkTimesReview;
import ru.fakegeek.rxretrodagsample.nytreviews.data.model.UrlLink;

/**
 *
 * Created by Antonov.Sergey6 on 16.08.2016.
 */
public abstract class NewYorkTimesTable {
    public static final String TABLE_NAME = "nyt_reviews_list";

    public static final String COLUMN_TITLE = "column_title";
    public static final String COLUMN_MPAA_RATING = "postamat_id";
    public static final String COLUMN_CRITICS_PICK = "critics_pick";
    public static final String COLUMN_HEADLINE = "headline";
    public static final String COLUMN_SUMMARY_SHORT = "summary_short";
    public static final String COLUMN_PUBLICATION_DATE = "publication_date";
    public static final String COLUMN_REVIEW_URL = "review_url";
    public static final String COLUMN_PHOTO = "photo";

    public static final String CREATE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    COLUMN_TITLE + " TEXT PRIMARY KEY, " +
                    COLUMN_MPAA_RATING + " TEXT NOT NULL, " +
                    COLUMN_CRITICS_PICK + " TEXT NOT NULL, " +
                    COLUMN_HEADLINE + " TEXT NOT NULL, " +
                    COLUMN_SUMMARY_SHORT + " TEXT NOT NULL, " +
                    COLUMN_PUBLICATION_DATE + " TEXT NOT NULL, " +
                    COLUMN_REVIEW_URL + " TEXT NOT NULL, " +
                    COLUMN_PHOTO + " TEXT NOT NULL" +
                    " ); ";

    public static ContentValues toContentValues(NewYorkTimesReview newYorkTimesReview) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE, newYorkTimesReview.title());
        values.put(COLUMN_MPAA_RATING, newYorkTimesReview.rating());
        values.put(COLUMN_CRITICS_PICK, newYorkTimesReview.criticsPick());
        values.put(COLUMN_HEADLINE, newYorkTimesReview.headline());
        values.put(COLUMN_SUMMARY_SHORT, newYorkTimesReview.summaryShort());
        values.put(COLUMN_PUBLICATION_DATE, newYorkTimesReview.publicationDate());
        values.put(COLUMN_REVIEW_URL, newYorkTimesReview.movieReviewUrl().url());
        values.put(COLUMN_PHOTO, newYorkTimesReview.multimediaPhoto().linkToPhoto());

        return values;
    }

    public static NewYorkTimesReview parseCursor(Cursor cursor) {
        MultimediaPhoto multimediaPhoto = MultimediaPhoto
                .create(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_PHOTO)));
        UrlLink link = UrlLink.create(cursor.getString(cursor
                .getColumnIndexOrThrow(COLUMN_REVIEW_URL)));

        return NewYorkTimesReview.builder()
                .setTitle(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TITLE)))
                .setRating(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_MPAA_RATING)))
                .setCriticsPick(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_CRITICS_PICK)))
                .setHeadline(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_HEADLINE)))
                .setSummaryShort(cursor.getString(cursor
                        .getColumnIndexOrThrow(COLUMN_SUMMARY_SHORT)))
                .setPublicationDate(cursor.getString(cursor
                        .getColumnIndexOrThrow(COLUMN_PUBLICATION_DATE)))
                .setMovieReviewUrl(link)
                .setMultimediaPhoto(multimediaPhoto)
                .build();
    }
}
