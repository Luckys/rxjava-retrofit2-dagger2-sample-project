package ru.fakegeek.rxretrodagsample.nytreviews.data.remote;

import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.fakegeek.rxretrodagsample.nytreviews.data.model.Results;
import rx.Observable;

/**
 *
 * Created by serg on 11.09.16.
 */
public interface ApiNewYorkTimes {

    // Since we have different API examples, we use unique endpoint for every API
    String ENDPOINT = "https://api.nytimes.com/";

    @GET("svc/movies/v2/reviews/search.json")
    Observable<Results> getReviews(@Query("api-key") String apiKey,
                                    @Query("query") String searchQuery);
}
