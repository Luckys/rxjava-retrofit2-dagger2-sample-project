package ru.fakegeek.rxretrodagsample.nytreviews.data;

import android.support.annotation.Nullable;

import java.util.List;

import javax.inject.Inject;

import hugo.weaving.DebugLog;
import ru.fakegeek.rxretrodagsample.BuildConfig;
import ru.fakegeek.rxretrodagsample.nytreviews.data.local.NewYorkTimesDbHelper;
import ru.fakegeek.rxretrodagsample.nytreviews.data.model.NewYorkTimesReview;
import ru.fakegeek.rxretrodagsample.nytreviews.data.model.Results;
import ru.fakegeek.rxretrodagsample.nytreviews.data.remote.ApiNewYorkTimes;
import rx.Observable;

/**
 *
 * Created by Antonov.Sergey6 on 16.08.2016.
 */
public class NewYorkTimesDataManager {

    private final NewYorkTimesDbHelper mNewYorkTimesDbHelper;
    private final ApiNewYorkTimes mApiNewYorkTimes;

    @Inject
    public NewYorkTimesDataManager(NewYorkTimesDbHelper newYorkTimesDbHelper,
                                   ApiNewYorkTimes apiNewYorkTimes) {
        this.mNewYorkTimesDbHelper = newYorkTimesDbHelper;
        this.mApiNewYorkTimes = apiNewYorkTimes;
    }

    @DebugLog
    public Observable<Results> syncNewYorkTimesReviews(@Nullable String searchQuery) {
        String nytApiKey = BuildConfig.NYT_API_KEY;
        return mApiNewYorkTimes.getReviews(nytApiKey, searchQuery);
    }

    @DebugLog
    public Observable<List<NewYorkTimesReview>> getNewYorkTimesReviews() {
        return mNewYorkTimesDbHelper.getNewYorkTimesReviews();
    }

    @DebugLog
    public void setNewYorkTimesReviews(List<NewYorkTimesReview> newYorkTimesReviews) {
        mNewYorkTimesDbHelper.setNewYorkTimesReviews(newYorkTimesReviews);
    }

    @DebugLog
    public void deleteNewYorkTimesReviews() {
        mNewYorkTimesDbHelper.deleteNewYorkTimesReviews();
    }
}
