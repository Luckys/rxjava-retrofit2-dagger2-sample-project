package ru.fakegeek.rxretrodagsample.nytreviews.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.fakegeek.rxretrodagsample.R;
import ru.fakegeek.rxretrodagsample.nytreviews.data.model.NewYorkTimesReview;


public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ReviewsViewHolder> {

    // --------------------------------------------------------------------------------------------
    //      FIELDS
    // --------------------------------------------------------------------------------------------

    private List<NewYorkTimesReview> mReviews;
    private ReviewClickListener mReviewClickListener;

    // --------------------------------------------------------------------------------------------
    //      CONSTRUCTOR
    // --------------------------------------------------------------------------------------------

    @Inject
    public ReviewsAdapter() {
        mReviews = new ArrayList<>();
    }

    // --------------------------------------------------------------------------------------------
    //      VIEW HOLDER
    // --------------------------------------------------------------------------------------------

    class ReviewsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_review) ImageView mImageViewReview;
        @BindView(R.id.text_title) TextView titleTextView;
        @BindView(R.id.text_date) TextView dateTextView;
        @BindView(R.id.text_rating) TextView ratingTextView;
        @BindView(R.id.text_critics_pick) TextView criticksTextView;
        @BindView(R.id.text_headline) TextView headlineTextView;
        @BindView(R.id.text_summary_short) TextView summaryTextView;
        @BindView(R.id.btn_read_more) Button readMoreButton;

        public ReviewsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ReviewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_reviews, parent, false);
        return new ReviewsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ReviewsViewHolder holder, int position) {
        NewYorkTimesReview review = mReviews.get(position);

        String linkToPhoto = "";
        if (review.multimediaPhoto() != null) {
            linkToPhoto = review.multimediaPhoto().linkToPhoto();
        } else {
            holder.mImageViewReview.setBackgroundResource(getRandomColor());
        }
        Glide
                .with(holder.mImageViewReview.getContext())
                .load(linkToPhoto)
                .centerCrop()
                .placeholder(R.drawable.fakegeek_912_512)
                .crossFade()
                .into(holder.mImageViewReview);

        holder.titleTextView.setText(review.title());
        holder.dateTextView.setText(review.publicationDate());

        if (review.rating().equals("")) {
            holder.ratingTextView.setVisibility(View.INVISIBLE);
        } else {
            holder.ratingTextView.setText(review.rating());
        }

        if (review.criticsPick().equals("0")) {
            holder.criticksTextView.setVisibility(View.INVISIBLE);
        } else {
            holder.criticksTextView.setText(R.string.criticks_pick);
        }

        holder.headlineTextView.setText(review.headline());
        holder.summaryTextView.setText(review.summaryShort());

        holder.readMoreButton.setOnClickListener(v -> {
            if (mReviewClickListener != null) {
                mReviewClickListener.onReviewClicked(review);
            }
        });
    }

    // --------------------------------------------------------------------------------------------
    //      PUBLIC METHODS
    // --------------------------------------------------------------------------------------------

    public void setOnReviewClickListener(ReviewClickListener reviewClickListener) {
        this.mReviewClickListener = reviewClickListener;
    }

    public void setReviews(List<NewYorkTimesReview> reviews) {
        mReviews = reviews;
    }

    @Override
    public int getItemCount() {
        return mReviews.size();
    }

    // --------------------------------------------------------------------------------------------
    //      PRIVATE METHODS
    // --------------------------------------------------------------------------------------------

    private int getRandomColor() {

        Integer[] colors = new Integer[] {
                R.color.colorDeepPurple,
                R.color.colorGreen,
                R.color.colorIndigo,
                R.color.colorRed,
                R.color.colorTeal,
                R.color.colorPurple,
                R.color.colorPink
        };

        return colors[new Random().nextInt(colors.length)];
    }

    // --------------------------------------------------------------------------------------------
    //      INTERFACE
    // --------------------------------------------------------------------------------------------

    public interface ReviewClickListener {
        void onReviewClicked(NewYorkTimesReview review);
    }
}
