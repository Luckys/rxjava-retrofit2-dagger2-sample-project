package ru.fakegeek.rxretrodagsample.nytreviews.data.model;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import java.util.List;

/**
 *
 * Created by serg on 12.09.16.
 */
@AutoValue
public abstract class Results implements Parcelable {

    public abstract List<NewYorkTimesReview> results();

    public static Results create(List<NewYorkTimesReview> results) {
        return new AutoValue_Results(results);
    }

    public static TypeAdapter<Results> typeAdapter(Gson gson) {
        return new AutoValue_Results.GsonTypeAdapter(gson);
    }
}
