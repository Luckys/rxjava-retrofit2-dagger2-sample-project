package ru.fakegeek.rxretrodagsample.nytreviews.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.fakegeek.rxretrodagsample.R;
import ru.fakegeek.rxretrodagsample.core.data.SyncService;
import ru.fakegeek.rxretrodagsample.core.ui.base.BaseActivity;
import ru.fakegeek.rxretrodagsample.nytreviews.data.model.NewYorkTimesReview;
import ru.fakegeek.rxretrodagsample.nytreviews.ui.adapter.ReviewsAdapter;
import ru.fakegeek.rxretrodagsample.util.DialogFactory;

public class ReviewsActivity extends BaseActivity implements ReviewsView {

    // --------------------------------------------------------------------------------------------
    //      FIELDS
    // --------------------------------------------------------------------------------------------

    private static final String EXTRA_TRIGGER_SYNC_FLAG =
            ReviewsActivity.class.getCanonicalName() + "EXTRA_TRIGGER_SYNC_FLAG";

    @Inject ReviewsPresenter mReviewsPresenter;
    @Inject ReviewsAdapter mReviewsAdapter;

    // --------------------------------------------------------------------------------------------
    //      UI REFERENCES
    // --------------------------------------------------------------------------------------------

    @BindView(R.id.recycler_view) RecyclerView mRecyclerView;
    @BindView(R.id.swipe_refresh) SwipeRefreshLayout mSwipeRefreshLayout;

    /**
     * Return an Intent to start this Activity.
     * triggerDataSyncOnCreate allows disabling the background sync service onCreate. Should
     * only be set to false during testing.
     */
    public static Intent getStartIntent(Context context, boolean triggerDataSyncOnCreate) {
        Intent intent = new Intent(context, ReviewsActivity.class);
        intent.putExtra(EXTRA_TRIGGER_SYNC_FLAG, triggerDataSyncOnCreate);
        return intent;
    }

    // --------------------------------------------------------------------------------------------
    //      LIFECYCLE
    // --------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_reviews);
        ButterKnife.bind(this);

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mReviewsAdapter.setOnReviewClickListener(review -> mReviewsPresenter.openLink(review));
        mSwipeRefreshLayout.setOnRefreshListener(() -> mReviewsPresenter.loadReviews(null));
    }

    @Override
    protected void onPause() {
        super.onPause();

        mReviewsAdapter.setOnReviewClickListener(null);
        mSwipeRefreshLayout.setOnRefreshListener(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mReviewsPresenter.detachView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_reviews, menu);

        MenuItem searchItem = menu.findItem( R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mReviewsPresenter.loadReviews(query);
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                searchItem.collapseActionView();
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                return false;
            }
        });
        return true;
    }

    // --------------------------------------------------------------------------------------------
    //      PRIVATE METHODS
    // --------------------------------------------------------------------------------------------

    private void init() {
        mRecyclerView.setAdapter(mReviewsAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mReviewsPresenter.attachView(this);
        mReviewsPresenter.loadReviews(null);

        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_purple,
                android.R.color.holo_green_light,
                android.R.color.holo_blue_bright,
                android.R.color.holo_red_light);

        if (getIntent().getBooleanExtra(EXTRA_TRIGGER_SYNC_FLAG, true)) {
            startService(SyncService.getStartIntent(this));
        }
    }

    // --------------------------------------------------------------------------------------------
    //      MVP VIEW METHODS IMPLEMENTATION
    // --------------------------------------------------------------------------------------------

    @Override
    public void showReviews(List<NewYorkTimesReview> reviews) {
        mReviewsAdapter.setReviews(reviews);
        mReviewsAdapter.notifyDataSetChanged();
    }

    @Override
    public void showError(String errorMessage) {
        DialogFactory.createGenericErrorDialog(this, errorMessage).show();
    }

    @Override
    public void showReviewsEmpty() {
        mReviewsAdapter.setReviews(Collections.<NewYorkTimesReview>emptyList());
        mReviewsAdapter.notifyDataSetChanged();
        Toast.makeText(this, R.string.empty_reviews, Toast.LENGTH_LONG).show();
    }

    @Override
    public void stopRefreshing() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void openLink(NewYorkTimesReview review) {

        Uri reviewLink = Uri.parse(review.movieReviewUrl().url());

        // create an intent builder
        CustomTabsIntent.Builder intentBuilder = new CustomTabsIntent.Builder();

        intentBuilder.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        intentBuilder
                .setSecondaryToolbarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        // set start and exit animations
        intentBuilder.setStartAnimations(this, android.R.anim.slide_in_left,
                android.R.anim.fade_in);
        intentBuilder.setExitAnimations(this, android.R.anim.fade_out,
                android.R.anim.slide_out_right);

        // build custom tabs intent
        CustomTabsIntent customTabsIntent = intentBuilder.build();

        // launch the url
        customTabsIntent.launchUrl(this, reviewLink);
    }
}
