package ru.fakegeek.rxretrodagsample.nytreviews.data.model;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;


/**
 *
 * Created by serg on 10.09.16.
 */
@AutoValue
public abstract class NewYorkTimesReview implements Parcelable {

    @SerializedName("display_title")
    public abstract String title();

    @SerializedName("mpaa_rating")
    public abstract String rating();

    @SerializedName("critics_pick")
    public abstract String criticsPick();

    @SerializedName("headline")
    public abstract String headline();

    @SerializedName("summary_short")
    public abstract String summaryShort();

    @SerializedName("publication_date")
    public abstract String publicationDate();

    @SerializedName("link")
    public abstract UrlLink movieReviewUrl();

    @SerializedName("multimedia")
    public abstract @Nullable MultimediaPhoto multimediaPhoto();

    public static Builder builder() {
        return new AutoValue_NewYorkTimesReview.Builder();
    }

    public static TypeAdapter<NewYorkTimesReview> typeAdapter(Gson gson) {
        return new AutoValue_NewYorkTimesReview.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder setTitle(String title);
        public abstract Builder setRating(String rating);
        public abstract Builder setCriticsPick(String criticsPick);
        public abstract Builder setHeadline(String headline);
        public abstract Builder setSummaryShort(String summaryShort);
        public abstract Builder setPublicationDate(String publicationDate);
        public abstract Builder setMovieReviewUrl(UrlLink movieReviewUrl);
        public abstract Builder setMultimediaPhoto(@Nullable MultimediaPhoto multimediaPhoto);
        public abstract NewYorkTimesReview build();
    }
}
