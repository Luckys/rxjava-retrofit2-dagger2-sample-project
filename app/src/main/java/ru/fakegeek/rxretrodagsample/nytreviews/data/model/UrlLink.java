package ru.fakegeek.rxretrodagsample.nytreviews.data.model;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import java.util.List;

/**
 *
 * Created by serg on 12.09.16.
 */
@AutoValue
public abstract class UrlLink  implements Parcelable {

    public abstract String url();

    public static UrlLink create(String url) {
        return new AutoValue_UrlLink(url);
    }

    public static TypeAdapter<UrlLink> typeAdapter(Gson gson) {
        return new AutoValue_UrlLink.GsonTypeAdapter(gson);
    }
}
