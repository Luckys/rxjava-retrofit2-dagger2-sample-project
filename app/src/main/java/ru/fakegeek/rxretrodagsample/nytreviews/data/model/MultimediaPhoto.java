package ru.fakegeek.rxretrodagsample.nytreviews.data.model;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

/**
 *
 * Created by serg on 10.09.16.
 */
@AutoValue
public abstract class MultimediaPhoto implements Parcelable {

    @SerializedName("src")
    public abstract String linkToPhoto();

    public static MultimediaPhoto create(@Nullable String linkToPhoto) {
        if (linkToPhoto == null) {
            linkToPhoto = "";
        }
        return new AutoValue_MultimediaPhoto(linkToPhoto);
    }

    public static TypeAdapter<MultimediaPhoto> typeAdapter(Gson gson) {
        return new AutoValue_MultimediaPhoto.GsonTypeAdapter(gson);
    }
}
