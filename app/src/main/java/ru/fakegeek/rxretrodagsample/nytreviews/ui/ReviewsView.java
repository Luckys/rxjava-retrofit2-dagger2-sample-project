package ru.fakegeek.rxretrodagsample.nytreviews.ui;

import java.util.List;

import ru.fakegeek.rxretrodagsample.nytreviews.data.model.NewYorkTimesReview;
import ru.fakegeek.rxretrodagsample.core.ui.base.MvpView;

/**
 *
 * Created by serg on 11.09.16.
 */
public interface ReviewsView extends MvpView {

    void showReviews(List<NewYorkTimesReview> reviews);

    void showReviewsEmpty();

    void stopRefreshing();

    void showError(String errorMessage);

    void openLink(NewYorkTimesReview review);
}
