package ru.fakegeek.rxretrodagsample.injection.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import ru.fakegeek.rxretrodagsample.core.data.SyncService;
import ru.fakegeek.rxretrodagsample.core.data.local.PreferencesHelper;
import ru.fakegeek.rxretrodagsample.injection.ApplicationContext;
import ru.fakegeek.rxretrodagsample.injection.module.ApplicationModule;
import ru.fakegeek.rxretrodagsample.nytreviews.data.NewYorkTimesDataManager;
import ru.fakegeek.rxretrodagsample.nytreviews.data.local.NewYorkTimesDbHelper;
import ru.fakegeek.rxretrodagsample.nytreviews.data.remote.ApiNewYorkTimes;
import ru.fakegeek.rxretrodagsample.util.RxEventBus;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(SyncService syncService);

    @ApplicationContext
    Context context();
    Application application();
    PreferencesHelper preferencesHelper();
    RxEventBus eventBus();

    ApiNewYorkTimes apiNewYorkTimes();
    NewYorkTimesDbHelper newYorkTimesDbHelper();
    NewYorkTimesDataManager newYorkTimesDataManager();
}
