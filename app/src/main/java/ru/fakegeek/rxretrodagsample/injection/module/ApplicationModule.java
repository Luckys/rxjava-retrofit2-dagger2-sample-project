package ru.fakegeek.rxretrodagsample.injection.module;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.fakegeek.rxretrodagsample.core.network.ApiCreatorHelper;
import ru.fakegeek.rxretrodagsample.injection.ApplicationContext;
import ru.fakegeek.rxretrodagsample.nytreviews.data.remote.ApiNewYorkTimes;

/**
 * Provide application-level dependencies.
 */
@Module
public class ApplicationModule {
    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    ApiNewYorkTimes apiNewYorkTimes() {
        return ApiCreatorHelper.createNewApiCall(ApiNewYorkTimes.class, ApiNewYorkTimes.ENDPOINT);
    }
}
