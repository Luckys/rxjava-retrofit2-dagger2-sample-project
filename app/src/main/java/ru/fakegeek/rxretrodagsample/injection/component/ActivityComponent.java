package ru.fakegeek.rxretrodagsample.injection.component;

import dagger.Subcomponent;
import ru.fakegeek.rxretrodagsample.injection.PerActivity;
import ru.fakegeek.rxretrodagsample.injection.module.ActivityModule;
import ru.fakegeek.rxretrodagsample.nytreviews.ui.ReviewsActivity;

/**
 * This component inject dependencies to all Activities across the application
 */
@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(ReviewsActivity reviewsActivity);
}
